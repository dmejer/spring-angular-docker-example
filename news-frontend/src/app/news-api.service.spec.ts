import { TestBed, inject } from '@angular/core/testing';

import { NewsApiService } from './news-api.service';
import { HttpClientModule } from '@angular/common/http';

describe('NewsApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [NewsApiService]
    });
  });

  it('should be created', inject([NewsApiService], (service: NewsApiService) => {
    expect(service).toBeTruthy();
  }));
});
