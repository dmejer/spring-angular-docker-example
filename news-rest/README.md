# News-rest module

## What is it?
News-rest module gets news from [news api](https://newsapi.org/) and public them on REST service.

## System requirements
JDK 8, maven, docker and news API key.
You can get news API key (NEWS_API_KEY) from [news api](https://newsapi.org/).
API_KEY is passed by enviroment variable: news-api.apikey.
Please replace NEWS_API_KEY with your api key.

## Build
```sh
$ mvn install -Dnews-api.apikey="NEWS_API_KEY"
```

## REST api documentation
After build, documentation is available in `target/generated-docs` folder.

## Running development server
```sh
$ mvn spring-boot:run -Dnews-api.apikey="NEWS_API_KEY"
```

## Running docker image from GitLab repository
```sh
$ docker run -p 8080:8080 -e news-api.apikey="NEWS_API_KEY" -it registry.gitlab.com/dmejer/spring-angular-docker-example/news-rest:master
```
