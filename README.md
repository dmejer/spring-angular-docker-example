[![pipeline status](https://gitlab.com/dmejer/spring-angular-docker-example/badges/master/pipeline.svg)](https://gitlab.com/dmejer/spring-angular-docker-example/commits/master)
# Spring, angular, docker, microservices example

This is a simple application to demostrate spring, rest, angular, docker usage. There are two small applications (microservices):
* news rest 
* news frontend

Application consumes and serves news to frontend using [news api](https://newsapi.org).

Frameworks used in application:
* Spring 5
* Angular 6
* Docker 18.x

# [News - rest usage details](news-rest/README.md)

# [News - frontend usage details](news-frontend/README.md)
