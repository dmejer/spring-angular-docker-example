import { Injectable } from '@angular/core';
import { News } from './news';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NewsApiServiceIf } from './news-api-service-if';

@Injectable({
  providedIn: 'root'
})
export class NewsApiService implements NewsApiServiceIf {

  constructor(private http: HttpClient) { }

  public getNews(category: string, page: number): Observable<News> {
    return this.http.get<News>('/news/pl/' + category + '/?page=' + page + '&size=10');
  }

}
