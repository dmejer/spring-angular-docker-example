import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from "rxjs";
import { News } from './news';
import { NewsApiServiceIf } from './news-api-service-if';

@Injectable({
  providedIn: 'root'
})
export class NewsApiMockService implements NewsApiServiceIf {

  constructor() { }

  public getNews(category: string, page: number): Observable<News> {
    return of(
      new News({
        "status": "ok",
        "totalResults": 2,
        "articles": [
          {
            "source": {
              "id": null,
              "name": "Kron4.com"
            },
            "author": "Alexa Mae Asperin",
            "title": "Tyson recalls frozen chicken over possible plastic contamination",
            "description": "There have been no confirmed reports of injuries.",
            "url": "http://www.kron4.com/news/national/tyson-recalls-frozen-chicken-over-possible-plastic-contamination/1228438720",
            "urlToImage": "https://media.kron.com/nxs-krontv-media-us-east-1/photo/2017/03/06/tyson_32851775_ver1.0_1280_720.jpg",
            "publishedAt": "2018-06-09T15:36:10Z"
          },
          {
            "source": {
              "id": null,
              "name": "Thestar.com"
            },
            "author": null,
            "title": "Tesla Model X sped up before fatal crash in California",
            "description": "The investigation from the U.S. National Transportation Safety Board is the latest to shine light on potential flaws in autonomous driving technology.",
            "url": "https://www.thestar.com/business/tech_news/2018/06/09/tesla-model-x-sped-up-before-fatal-crash-in-california.html",
            "urlToImage": "https://images.thestar.com/M6dbqbqsrNvrj47d7_0XEKcamzw=/1200x621/smart/filters:cb(1528558157049)/https://www.thestar.com/content/dam/thestar/business/tech_news/2018/06/09/tesla-model-x-sped-up-before-fatal-crash-in-california/mountainviewcrash.jpg",
            "publishedAt": "2018-06-09T14:11:58Z"
          }
        ]
      }
      ));
  };


}
