#!/bin/sh

echo "NEWS_APIURL: " $NEWS_API_URL
envsubst '\$NEWS_API_URL' < /usr/src/local/nginx-main.conf > /etc/nginx/conf.d/default.conf 

# Start nginx
nginx -g 'daemon off;'