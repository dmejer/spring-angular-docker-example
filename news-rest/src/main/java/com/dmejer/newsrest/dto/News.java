package com.dmejer.newsrest.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;

@JsonIgnoreProperties(ignoreUnknown = true)
public class News {

	private String status;
	private int totalResults;
	private List<Article> articles;

	private News() {
		articles = Lists.newArrayList();
	}

	public String getStatus() {
		return status;
	}

	private void setStatus(String status) {
		this.status = status;
	}

	public int getTotalResults() {
		return totalResults;
	}

	private void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public List<Article> getArticles() {
		return Lists.newArrayList(articles);
	}

	public static class Builder {

		private News obj;

		public Builder() {
			this.obj = new News();
		}

		public Builder withStatus(String status) {
			this.obj.setStatus(status);
			return this;
		}

		public Builder withTotalResults(int totalResults) {
			this.obj.setTotalResults(totalResults);
			return this;
		}

		public Builder addArticle(Article article) {
			this.obj.articles.add(article);
			return this;
		}

		public News build() {
			return this.obj;
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((articles == null) ? 0 : articles.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + totalResults;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (articles == null) {
			if (other.articles != null)
				return false;
		} else if (!articles.equals(other.articles))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (totalResults != other.totalResults)
			return false;
		return true;
	}

}
