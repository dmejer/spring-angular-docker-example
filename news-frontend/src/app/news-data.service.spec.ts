import { TestBed, inject } from '@angular/core/testing';

import { NewsDataService } from './news-data.service';
import { News } from './news';
import { NewsApiService } from './news-api.service';
import { NewsApiMockService } from './news-api-mock.service';

describe('NewsDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewsDataService,
        {
          provide: NewsApiService,
          useClass: NewsApiMockService
        }
      ]
    });
  });

  it('should be created', inject([NewsDataService], (service: NewsDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getNews()', () => {
    it('should return all news', inject([NewsDataService], (service: NewsDataService) => {
      service.getNews('technology', 1).subscribe(news => {
        this.news = news;
      });

      expect(this.news.articles[0].title)
        .toEqual("Tyson recalls frozen chicken over possible plastic contamination");
      expect(this.news.articles[1].title)
        .toEqual("Tesla Model X sped up before fatal crash in California");
    }
    ));

  });

});
