import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { NewsDataService } from './news-data.service';
import { NewsApiService } from './news-api.service';
import { NewsApiMockService } from './news-api-mock.service';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        NewsDataService,
        {
          provide: NewsApiService,
          useClass: NewsApiMockService
        }
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render news in a articles-list', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#news-status').textContent).toEqual('status: ok');
  }));
});
