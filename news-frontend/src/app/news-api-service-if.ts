import { Observable } from "rxjs";
import { News } from "./news";

export interface NewsApiServiceIf {
    getNews(category: string, page: number): Observable<News>;
}
