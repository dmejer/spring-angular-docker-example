package com.dmejer.newsrest.api.exception;

@SuppressWarnings("serial")
public class InternalErrorNewsException extends Exception {

	public InternalErrorNewsException(String message) {
		super(message);
	}

}
