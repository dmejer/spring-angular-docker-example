import { Injectable } from '@angular/core';
import { News } from './news';
import { NewsApiService } from './news-api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsDataService {

  constructor(private api: NewsApiService) {
  }

  getNews(category: string, page: number): Observable<News> {
    return this.api.getNews(category, page);
  }
}
