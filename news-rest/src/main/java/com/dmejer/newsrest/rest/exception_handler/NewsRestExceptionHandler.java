package com.dmejer.newsrest.rest.exception_handler;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.dmejer.newsrest.api.exception.BadRequestNewsException;
import com.dmejer.newsrest.api.exception.InternalErrorNewsException;
import com.dmejer.newsrest.api.exception.WrongApiKeyNewsException;
import com.dmejer.newsrest.rest.NewsRestController;

@ControllerAdvice(assignableTypes = NewsRestController.class)
public class NewsRestExceptionHandler {

	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(WrongApiKeyNewsException.class)
	@ResponseBody
	public ErrorInfo handleWrongApiKeyNewsException() {
		return new ErrorInfo(HttpStatus.UNAUTHORIZED,
				"Wrong news API key. Please go to https://newsapi.org/, to get api key.");
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BadRequestNewsException.class)
	@ResponseBody
	public ErrorInfo handleBadRequestNewsException() {
		return new ErrorInfo(HttpStatus.BAD_REQUEST, "Bad request.");
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = { InternalErrorNewsException.class, Exception.class })
	@ResponseBody
	public ErrorInfo handleInternalErrorNewsException(Exception exception) {
		final String message;
		if (exception != null && exception.getMessage() != null)
			message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
		else
			message = "Unknown error";
		return new ErrorInfo(HttpStatus.INTERNAL_SERVER_ERROR, message);
	}

}
