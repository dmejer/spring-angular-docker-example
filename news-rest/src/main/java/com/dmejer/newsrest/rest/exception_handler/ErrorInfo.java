package com.dmejer.newsrest.rest.exception_handler;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ErrorInfo {

	private final HttpStatus status;
	private final String message;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private final Date timestamp;

	public ErrorInfo() {
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.message = "";
		timestamp = new Date();
	}

	public ErrorInfo(HttpStatus status, String message) {
		this.status = status;
		this.message = message;
		timestamp = new Date();
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public Date getTimestamp() {
		return new Date(timestamp.getTime());
	}

}