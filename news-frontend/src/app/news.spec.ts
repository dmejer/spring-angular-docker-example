import { News } from './news';

describe('News', () => {
  it('should create an instance', () => {
    expect(new News()).toBeTruthy();
  });

  it('should return null when json is null', () => {
    let news = new News(null);
    expect(news).toEqual(new News());
  });

  it('should create an instance from obj', () => {
    let news = new News({ status: 'ok', totalResults: 2 });
    expect(news.status).toEqual('ok');
    expect(news.totalResults).toEqual(2);
  });

});
