package com.dmejer.newsrest.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class HttpConfig {

	@Bean
	@Qualifier("newsApiRestTemplateCustomizer")
	public NewsApiRestTemplateCustomizer newsApiRestTemplateCustomizer() {
		return new NewsApiRestTemplateCustomizer();
	}

	@Bean
	@DependsOn(value = { "newsApiRestTemplateCustomizer" })
	public RestTemplateBuilder restTemplateBuilder() {
		return new RestTemplateBuilder(newsApiRestTemplateCustomizer());
	}

}