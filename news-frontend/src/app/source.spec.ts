import { Source } from './source';

describe('Source', () => {
  it('should create an instance', () => {
    expect(new Source()).toBeTruthy();
  });


  it('should create an instance from obj', () => {
    let news = new Source({ id: '1', name: 'name A' });
    expect(news.id).toEqual('1');
    expect(news.name).toEqual('name A');
  });

});
