import { Source } from "./source";

export class Article {

    source: Source;
    author: string;
    title: string;
    description: string;
    url: string;
    urlToImage: string;
    publishedat: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}
