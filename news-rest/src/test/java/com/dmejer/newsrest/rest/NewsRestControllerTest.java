package com.dmejer.newsrest.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.dmejer.newsrest.api.NewsApiService;
import com.dmejer.newsrest.api.exception.BadRequestNewsException;
import com.dmejer.newsrest.api.exception.InternalErrorNewsException;
import com.dmejer.newsrest.api.exception.WrongApiKeyNewsException;
import com.dmejer.newsrest.dto.Article;
import com.dmejer.newsrest.dto.News;
import com.dmejer.newsrest.dto.News.Builder;
import com.dmejer.newsrest.dto.Source;
import com.dmejer.newsrest.rest.exception_handler.ErrorInfo;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(NewsRestController.class)
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class NewsRestControllerTest {

	private static final int PAGE_DEFAULT = 1;

	private static final int PAGESIZE_DEFAULT = 10;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private NewsApiService apiService;

	private JacksonTester<News> jsonNews;

	private JacksonTester<ErrorInfo> jsonError;

	@Before
	public void setup() {
		// Initializes the JacksonTester
		JacksonTester.initFields(this, new ObjectMapper());
	}

	@Test
	public void Should_GetBadRequest_When_ArgumentsWrong() throws Exception {
		// given
		given(apiService.getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT))
				.willThrow(new BadRequestNewsException());

		// when
		MockHttpServletResponse response = mockMvc.perform(get("/news/XX/XXX")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT);
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());

		ErrorInfo errorJson = jsonError.parse(response.getContentAsString()).getObject();
		assertThat(errorJson.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(errorJson.getMessage()).isEqualTo("Bad request.");
		assertThat(errorJson.getTimestamp()).isNotNull();
	}

	@Test
	public void Should_GetUnauthorized_When_WrongApiKey() throws Exception {
		// given
		given(apiService.getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT))
				.willThrow(new WrongApiKeyNewsException());

		// when
		MockHttpServletResponse response = mockMvc.perform(get("/news/XX/XXX")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT);
		assertThat(response.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());

		ErrorInfo errorJson = jsonError.parse(response.getContentAsString()).getObject();
		assertThat(errorJson.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED);
		assertThat(errorJson.getMessage())
				.isEqualTo("Wrong news API key. Please go to https://newsapi.org/, to get api key.");
		assertThat(errorJson.getTimestamp()).isNotNull();
	}

	@Test
	public void Should_GetInternalError_When_InternalErrorFromApi() throws Exception {
		// given
		given(apiService.getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT))
				.willThrow(new InternalErrorNewsException("internal error"));

		// when
		MockHttpServletResponse response = mockMvc.perform(get("/news/XX/XXX")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT);
		assertThat(response.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());

		ErrorInfo errorJson = jsonError.parse(response.getContentAsString()).getObject();
		assertThat(errorJson.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
		assertThat(errorJson.getMessage()).isEqualTo("internal error");
		assertThat(errorJson.getTimestamp()).isNotNull();
	}

	@Test
	public void Should_GetInternalError_When_Unexpected() throws Exception {
		// given
		given(apiService.getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT))
				.willThrow(new RuntimeException("some error"));

		// when
		MockHttpServletResponse response = mockMvc.perform(get("/news/XX/XXX")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT);
		assertThat(response.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());

		ErrorInfo errorJson = jsonError.parse(response.getContentAsString()).getObject();
		assertThat(errorJson.getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
		assertThat(errorJson.getMessage()).isEqualTo("some error");
		assertThat(errorJson.getTimestamp()).isNotNull();
	}

	@Test
	public void Should_GetCorrectJson_When_CountryAndCategoryAreOk() throws Exception {
		Builder newsBuilder = new News.Builder().withStatus("ok").withTotalResults(1);

		com.dmejer.newsrest.dto.Source.Builder sourceBuilder = new Source.Builder().withName("source, news site");

		com.dmejer.newsrest.dto.Article.Builder articleBuilder = new Article.Builder().withAuthor("Author field")
				.withTitle("some title").withDescription("Articles's description").withUrl("url to the article")
				.withSource(sourceBuilder.build());
		newsBuilder.addArticle(articleBuilder.build());

		News expected = newsBuilder.build();

		// given
		given(apiService.getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT)).willReturn(expected);

		// when
		MockHttpServletResponse response = mockMvc.perform(get("/news/XX/XXX")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT);
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonNews.write(expected).getJson());
	}

	@Test
	public void Should_GetPageAndSize_When_PageAndSizeWereLoadedFromDefault() throws Exception {
		// when
		mockMvc.perform(get("/news/XX/XXX")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", PAGE_DEFAULT, PAGESIZE_DEFAULT);
	}

	@Test
	public void Should_GetPageAndSize_When_PageAndSizeWereSet() throws Exception {
		// when
		mockMvc.perform(get("/news/XX/XXX?page=3&size=5")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", 3, 5);
	}

	@Test
	public void Should_GetPageAndSize_When_PageIsNullAndSizeIsSet() throws Exception {
		// when
		mockMvc.perform(get("/news/XX/XXX?size=5")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", PAGE_DEFAULT, 5);
	}

	@Test
	public void Should_GetPageAndSize_When_PageIsSetAndSizeIsNull() throws Exception {
		// when
		mockMvc.perform(get("/news/XX/XXX?page=5")).andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("XX", "XXX", 5, PAGESIZE_DEFAULT);
	}

	@Test
	public void Should_GetCorrectJsonAndMakeRestDocs_When_CountryAndCategoryAreOk() throws Exception {
		Builder newsBuilder = new News.Builder().withStatus("ok").withTotalResults(20);

		{
			com.dmejer.newsrest.dto.Source.Builder sourceBuilder = new Source.Builder().withName("Herald-review.com");

			com.dmejer.newsrest.dto.Article.Builder articleBuilder = new Article.Builder().withAuthor("")
					.withTitle("This week in odd news: Beer may lack fizz in Europe, rats eat $19000 from ATM")
					.withDescription(
							"From a lack of carbon dioxide that may lead to fizz-less beer in Europe to a group of rats that broke into an ATM for an expensive meal, here's a")
					.withUrl(
							"https://herald-review.com/news/national/this-week-in-odd-news-beer-may-lack-fizz-in/collection_065caad7-3071-5c69-ad26-fc6d403c9745.html")
					.withUrlToImage(
							"https://bloximages.chicago2.vip.townnews.com/herald-review.com/content/tncms/assets/v3/editorial/0/65/065caad7-3071-5c69-ad26-fc6d403c9745/5b2d3a02c785b.preview.jpg?crop=1763%2C992%2C0%2C91&resize=1120%2C630&order=crop%2Cresize")
					.withPublishedAt("2018-06-23T12:17:43Z").withSource(sourceBuilder.build());

			newsBuilder.addArticle(articleBuilder.build());
		}
		{
			com.dmejer.newsrest.dto.Source.Builder sourceBuilder = new Source.Builder().withId("reuters")
					.withName("Reuters");

			com.dmejer.newsrest.dto.Article.Builder articleBuilder = new Article.Builder().withAuthor("Ahmad Ghaddar")
					.withTitle("OPEC meets Russia, other allies to agree oil output boost")
					.withDescription(
							"OPEC is meeting Russia and other oil-producing allies on Saturday to clinch a new deal raising output, a day after agreeing a production hike within the group itself but confusing the market as to how much more oil it will pump.")
					.withUrl(
							"https://www.reuters.com/article/us-oil-opec/opec-meets-russia-other-allies-to-agree-oil-output-boost-idUSKBN1JJ07G")
					.withUrlToImage(
							"https://s3.reutersmedia.net/resources/r/?m=02&d=20180623&t=2&i=1275669834&w=1200&r=LYNXMPEE5M055")
					.withPublishedAt("2018-06-23T12:13:46Z").withSource(sourceBuilder.build());

			newsBuilder.addArticle(articleBuilder.build());
		}
		News expected = newsBuilder.build();

		// given
		given(apiService.getAllNews("us", "business", PAGE_DEFAULT, 2)).willReturn(expected);

		// when
		MockHttpServletResponse response = mockMvc
				.perform(RestDocumentationRequestBuilders.get("/news/{language}/{category}", "us", "business")
						.param("page", "1").param("size", "2"))
				.andDo(document("news-rest-example", preprocessResponse(prettyPrint()),
						pathParameters(parameterWithName("language").description("Selected language to query"),
								parameterWithName("category").description("Selected category to query")),
						requestParameters(parameterWithName("page").description("Page Number"),
								parameterWithName("size").description("Size of result"))))
				.andReturn().getResponse();

		// then
		verify(apiService, times(1)).getAllNews("us", "business", PAGE_DEFAULT, 2);
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		assertThat(response.getContentAsString()).isEqualTo(jsonNews.write(expected).getJson());
	}

}
