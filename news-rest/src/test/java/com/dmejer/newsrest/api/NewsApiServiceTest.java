package com.dmejer.newsrest.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;

import org.assertj.core.api.AbstractThrowableAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import com.dmejer.newsrest.api.exception.BadRequestNewsException;
import com.dmejer.newsrest.api.exception.InternalErrorNewsException;
import com.dmejer.newsrest.api.exception.WrongApiKeyNewsException;
import com.dmejer.newsrest.dto.Article;
import com.dmejer.newsrest.dto.News;
import com.dmejer.newsrest.dto.News.Builder;
import com.dmejer.newsrest.dto.Source;

@RunWith(SpringRunner.class)
@RestClientTest(NewsApiService.class)
@JsonTest
@TestPropertySource(properties = { "news-api.apikey=" })
public class NewsApiServiceTest {

	private static final String AUTHOR = "Matt Markovich";
	private static final String SOURCE = "Komonews.com";
	private static final String TITLE = "Seattle's new homeless plan would use City Hall lobby as overnight shelter";
	private static final String DESCRIPTION = "SEATTLE - A day after the Seattle City Council  repealed a tax on big businesses, a committee approved Mayor Jenny Durkan’s $13 million dollar plan to get 500 homeless individuals into new temporary and enhanced shelter in 90 days.Part of the plan includes";
	private static final String URL = "http://komonews.com/news/local/seattle-to-use-city-hall-lobby-as-overnight-homeless-shelter";

	@Autowired
	private MockRestServiceServer mockServer;

	@Autowired
	private NewsApiService apiService;

	@Autowired
	private JacksonTester<News> jsonNews;

	@Test
	public void Should_ThrowWrongApiKey_When_Unauthorized() throws BadRequestNewsException, InternalErrorNewsException {
		// given
		mockServer
				.expect(requestTo(
						"https://newsapi.org/v2/top-headlines?country=XX&category=XXX&apiKey=&page=1&pageSize=1"))
				.andRespond(withStatus(HttpStatus.UNAUTHORIZED));

		// when
		AbstractThrowableAssert<?, ? extends Throwable> exception = assertThatThrownBy(
				() -> apiService.getAllNews("XX", "XXX", 1, 1));

		// then
		mockServer.verify();
		exception.isInstanceOf(WrongApiKeyNewsException.class);
	}

	@Test
	public void Should_ThrowBadRequest_When_NoCountrylAndCategoryParam()
			throws BadRequestNewsException, InternalErrorNewsException, WrongApiKeyNewsException {
		// given
		mockServer
				.expect(requestTo("https://newsapi.org/v2/top-headlines?country=&category=&apiKey=&page=1&pageSize=1"))
				.andRespond(withStatus(HttpStatus.BAD_REQUEST));

		// when
		AbstractThrowableAssert<?, ? extends Throwable> exception = assertThatThrownBy(
				() -> apiService.getAllNews("", "", 1, 1));

		// then
		mockServer.verify();
		exception.isInstanceOf(BadRequestNewsException.class);
	}

	@Test
	public void Should_ThrowInternalErrorException_InternalErrorInNewsApi()
			throws BadRequestNewsException, InternalErrorNewsException, WrongApiKeyNewsException {
		// given
		mockServer
				.expect(requestTo("https://newsapi.org/v2/top-headlines?country=&category=&apiKey=&page=1&pageSize=1"))
				.andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR));

		// when
		AbstractThrowableAssert<?, ? extends Throwable> exception = assertThatThrownBy(
				() -> apiService.getAllNews("", "", 1, 1));

		// then
		mockServer.verify();
		exception.isInstanceOf(InternalErrorNewsException.class);
	}

	@Test
	public void Should_getCorrectResponse_When_CountryAndCategoryAreSet()
			throws WrongApiKeyNewsException, BadRequestNewsException, InternalErrorNewsException, IOException {
		Builder newsBuilder = new News.Builder().withStatus("ok").withTotalResults(1);

		com.dmejer.newsrest.dto.Source.Builder sourceBuilder = new Source.Builder().withName(SOURCE);
		com.dmejer.newsrest.dto.Article.Builder articlebuilder = new Article.Builder().withAuthor(AUTHOR)
				.withTitle(TITLE).withDescription(DESCRIPTION).withUrl(URL).withSource(sourceBuilder.build());

		newsBuilder.addArticle(articlebuilder.build());

		News news = newsBuilder.build();

		// given
		mockServer
				.expect(requestTo(
						"https://newsapi.org/v2/top-headlines?country=XX&category=XXX&apiKey=&page=1&pageSize=1"))
				.andRespond(withSuccess(jsonNews.write(news).getJson(), MediaType.APPLICATION_JSON_UTF8));

		// when
		apiService.getAllNews("XX", "XXX", 1, 1);

		// then
		mockServer.verify();
		assertThat(news).isNotNull().isEqualTo(news);
	}

}
