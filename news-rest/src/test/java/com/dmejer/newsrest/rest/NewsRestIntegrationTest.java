package com.dmejer.newsrest.rest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.dmejer.newsrest.dto.News;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class NewsRestIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void Should_GetNotFoundDefaultSpringResponse_When_ArgumentsAreNotSet() throws Exception {
		// when
		ResponseEntity<News> response = this.restTemplate.getForEntity("/news/{language}/{category}", News.class, "",
				"");

		// then
		assertThat(response).isNotNull();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody()).isNotNull();
	}

	@Test
	public void Should_GetResponseFromNewsApi_When_QueryForPlAndTechnology() throws Exception {
		// when
		ResponseEntity<News> response = this.restTemplate.getForEntity("/news/pl/technology?page=1&size=2", News.class);

		// then
		assertThat(response).isNotNull();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		News body = response.getBody();
		assertThat(body.getStatus()).isEqualTo("ok");
		assertThat(body.getTotalResults()).isNotNull().isPositive();
		assertThat(body.getArticles()).hasSize(2);
	}

	@Test
	public void Should_GetCorrectQuatityOfArticles_When_PagesizeWasSet() throws Exception {
		Should_GetCorrectQuatityOfArticles_When_PagesizeWasSetTo(0); // no articles
		Should_GetCorrectQuatityOfArticles_When_PagesizeWasSetTo(1);
		Should_GetCorrectQuatityOfArticles_When_PagesizeWasSetTo(6);
		Should_GetCorrectQuatityOfArticles_When_PagesizeWasSetTo(10);
	}

	private void Should_GetCorrectQuatityOfArticles_When_PagesizeWasSetTo(int pageSize) throws Exception {
		// when
		ResponseEntity<News> response = this.restTemplate
				.getForEntity("/news/pl/technology?page=1&size=" + String.valueOf(pageSize), News.class);

		// then
		assertThat(response).isNotNull();
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		News body = response.getBody();
		assertThat(body.getStatus()).isEqualTo("ok");
		assertThat(body.getTotalResults()).isNotNull().isPositive();
		assertThat(body.getArticles()).hasSize(pageSize);
	}

}
