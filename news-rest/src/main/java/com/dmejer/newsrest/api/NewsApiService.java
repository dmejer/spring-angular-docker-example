package com.dmejer.newsrest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.dmejer.newsrest.api.exception.BadRequestNewsException;
import com.dmejer.newsrest.api.exception.InternalErrorNewsException;
import com.dmejer.newsrest.api.exception.WrongApiKeyNewsException;
import com.dmejer.newsrest.dto.News;

@Service
@ConfigurationProperties(prefix = "news-api")
public class NewsApiService {

	private String newsApiUrl;
	private String apikey;

	private RestTemplate restTemplate;

	@Autowired
	public NewsApiService(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}

	public News getAllNews(String country, String category, int page, int pageSize)
			throws WrongApiKeyNewsException, BadRequestNewsException, InternalErrorNewsException {

		try {
			return restTemplate.getForObject(newsApiUrl, News.class, country, category, apikey, page, pageSize);
		} catch (HttpStatusCodeException e) {
			switch (e.getStatusCode()) {
			case UNAUTHORIZED:
				throw new WrongApiKeyNewsException();
			case BAD_REQUEST:
				throw new BadRequestNewsException();
			default:
				throw new InternalErrorNewsException(e.getMessage());
			}
		} catch (RuntimeException e) {
			throw new InternalErrorNewsException(e.getMessage());
		}
	}

	public String getNewsApiUrl() {
		return newsApiUrl;
	}

	public void setNewsApiUrl(String newsApiUrl) {
		this.newsApiUrl = newsApiUrl;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

}
