package com.dmejer.newsrest.dto;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.test.context.junit4.SpringRunner;

import com.dmejer.newsrest.dto.News.Builder;

@RunWith(SpringRunner.class)
@JsonTest
public class NewsDtoJsonTest {

	@Autowired
	private JacksonTester<News> json;

	@Test
	public void Should_SerializeToJson() throws IOException {
		// given
		Builder newsBuilder = new News.Builder().withStatus("ok").withTotalResults(20);
		{
			com.dmejer.newsrest.dto.Source.Builder sourceBuilder = new Source.Builder().withId("id")
					.withName("Source url");

			com.dmejer.newsrest.dto.Article.Builder articlebuilder = new Article.Builder().withAuthor("Author")
					.withTitle("Example title").withDescription("Example description").withUrl("Url to an article")
					.withUrlToImage("Url to an image").withPublishedAt("2018-06-24T12:19:43Z")
					.withSource(sourceBuilder.build());

			newsBuilder.addArticle(articlebuilder.build());
		}
		News newsDto = newsBuilder.build();

		// when
		JsonContent<News> content = this.json.write(newsDto);

		// then
		assertThat(content).extractingJsonPathStringValue("@.status").isEqualTo("ok");
		assertThat(content).extractingJsonPathNumberValue("@.totalResults").isEqualTo(20);
		assertThat(content).extractingJsonPathStringValue("@.articles[0].author").isEqualTo("Author");
		assertThat(content).extractingJsonPathStringValue("@.articles[0].title").isEqualTo("Example title");
		assertThat(content).extractingJsonPathStringValue("@.articles[0].description").isEqualTo("Example description");
		assertThat(content).extractingJsonPathStringValue("@.articles[0].url").isEqualTo("Url to an article");
		assertThat(content).extractingJsonPathStringValue("@.articles[0].urlToImage").isEqualTo("Url to an image");
		assertThat(content).extractingJsonPathStringValue("@.articles[0].publishedAt")
				.isEqualTo("2018-06-24T12:19:43Z");
		assertThat(content).extractingJsonPathStringValue("@.articles[0].source.id").isEqualTo("id");
		assertThat(content).extractingJsonPathStringValue("@.articles[0].source.name").isEqualTo("Source url");
	}

	@Test
	public void Should_DeserializeToObject() throws IOException {
		// given
		String JSON_OBJECT = "{\"status\":\"ok\",\"totalResults\":20,\"articles\":[{\"source\":{\"id\":null,\"name\":\"Telepolis.pl\"},\"author\":null,\"title\":\"Ambitne plany sprzedaży i bolesne zderzenie z rzeczywistością. Samsung nie zrealizuje planu?\",\"description\":\"Pierwotnie Samsung zakładał na bieżący rok sprzedaż na poziomie 320 milionów sztuk.\",\"url\":\"http://www.telepolis.pl/wiadomosci/ambitne-plany-sprzedazy-i-bolesne-zderzenie-z-rzeczywistoscia-samsung-nie-zrealizuje-planu,2,3,42822.html\",\"urlToImage\":\"http://www.telepolis.pl/grafika/miniatury/sad-samsung-1m.jpg\",\"publishedAt\":\"2018-06-24T12:12:03Z\"},{\"source\":{\"id\":null,\"name\":\"Tablety.pl\"},\"author\":\"Katarzyna Dąbkowska\",\"title\":\"iOS 11 jednak bez opcji pominięcia kodu blokady iPhone'ów. Apple wytyka błędy\",\"description\":\"Matthew Hickey, badacz bezpieczeństwa z firmy Hacker House, powiadomił w piątek, że iOS 11 pozwala na pominięcie kodu blokady poprzez podłączenie iPhone'a lub iPada do komputera z pomocą kabla Lightning. Apple szybko jednak wyłapało sprawę i wytknęło badaczow…\",\"url\":\"https://www.tablety.pl/apple_ios/2018-06-24/ios-11-apple-kod-blokady/\",\"urlToImage\":\"https://www.tablety.pl/wp-content/uploads/2018/06/apple-iphone.jpg\",\"publishedAt\":\"2018-06-24T12:07:30Z\"}]}";

		// when
		News obj = this.json.parse(JSON_OBJECT).getObject();

		// then
		assertThat(obj).isNotNull();
		assertThat(obj.getStatus()).isEqualTo("ok");
		assertThat(obj.getTotalResults()).isEqualTo(20);
		assertThat(obj.getArticles()).isNotNull().hasSize(2);
		{
			Article article = obj.getArticles().get(0);
			assertThat(article.getAuthor()).isNull();
			assertThat(article.getTitle()).isEqualTo(
					"Ambitne plany sprzedaży i bolesne zderzenie z rzeczywistością. Samsung nie zrealizuje planu?");
			assertThat(article.getDescription())
					.isEqualTo("Pierwotnie Samsung zakładał na bieżący rok sprzedaż na poziomie 320 milionów sztuk.");
			assertThat(article.getUrl()).isEqualTo(
					"http://www.telepolis.pl/wiadomosci/ambitne-plany-sprzedazy-i-bolesne-zderzenie-z-rzeczywistoscia-samsung-nie-zrealizuje-planu,2,3,42822.html");
			assertThat(article.getUrlToImage())
					.isEqualTo("http://www.telepolis.pl/grafika/miniatury/sad-samsung-1m.jpg");
			assertThat(article.getPublishedAt()).isEqualTo("2018-06-24T12:12:03Z");
			assertThat(article.getSource().getId()).isNull();
			assertThat(article.getSource().getName()).isEqualTo("Telepolis.pl");
		}
		{
			Article article = obj.getArticles().get(1);
			assertThat(article.getAuthor()).isEqualTo("Katarzyna Dąbkowska");
			assertThat(article.getTitle())
					.isEqualTo("iOS 11 jednak bez opcji pominięcia kodu blokady iPhone'ów. Apple wytyka błędy");
			assertThat(article.getDescription()).isEqualTo(
					"Matthew Hickey, badacz bezpieczeństwa z firmy Hacker House, powiadomił w piątek, że iOS 11 pozwala na pominięcie kodu blokady poprzez podłączenie iPhone'a lub iPada do komputera z pomocą kabla Lightning. Apple szybko jednak wyłapało sprawę i wytknęło badaczow…");
			assertThat(article.getUrl())
					.isEqualTo("https://www.tablety.pl/apple_ios/2018-06-24/ios-11-apple-kod-blokady/");
			assertThat(article.getUrlToImage())
					.isEqualTo("https://www.tablety.pl/wp-content/uploads/2018/06/apple-iphone.jpg");
			assertThat(article.getPublishedAt()).isEqualTo("2018-06-24T12:07:30Z");
			assertThat(article.getSource().getId()).isNull();
			assertThat(article.getSource().getName()).isEqualTo("Tablety.pl");
		}
	}
}
