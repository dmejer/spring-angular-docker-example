package com.dmejer.newsrest.config;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class NewsApiRestTemplateCustomizer implements RestTemplateCustomizer {

	@Value("${http.MAX_TOTAL_CONNECTIONS}")
	private int MAX_TOTAL_CONNECTIONS;

	/**
	 * the time to establish the connection with remote host
	 */
	@Value("${http.CONNECTION_TIMEOUT}")
	private int CONNECTION_TIMEOUT;

	/**
	 * timeout when requesting a connection from the connection manager
	 */
	@Value("${http.CONNECTION_REQUEST_TIMEOUT}")
	private int CONNECTION_REQUEST_TIMEOUT;

	/**
	 * timeout for waiting for data
	 */
	@Value("${http.SO_TIMEOUT}")
	private int SO_TIMEOUT;

	@Override
	public void customize(RestTemplate restTemplate) {
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient()));
	}

	public HttpClient httpClient() {
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(CONNECTION_TIMEOUT)
				.setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT).setSocketTimeout(SO_TIMEOUT).build();

		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

		HttpClient defaultHttpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig)
				.setConnectionManager(connectionManager).setMaxConnTotal(MAX_TOTAL_CONNECTIONS).build();

		return defaultHttpClient;
	}
}