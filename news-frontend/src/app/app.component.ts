import { Component, OnInit } from '@angular/core';
import { NewsDataService } from './news-data.service';
import { News } from './news';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NewsDataService]
})
export class AppComponent implements OnInit {

  _news: News;
  category: string;
  page: number;

  constructor(private newsDataService: NewsDataService) {
    this.category = 'technology';
    this.page = 1;
  }

  get news() {
    return this._news;
  }
  get categories() {
    return ['technology',
      'business',
      'health'];
  };

  increasePageNumber() {
    this.page++;

    this.loadNews();
  }

  deincreasePageNumber() {
    if (this.page > 1)
      this.page--;

    this.loadNews();
  }

  ngOnInit(): void {
    this.loadNews();
  }

  onChangeCategory(newValue) {
    this.category = newValue;
    this.loadNews();
  }

  loadNews() {
    this.newsDataService.getNews(this.category, this.page)
      .subscribe((response) => {
        this._news = response;
      });
  }

}
