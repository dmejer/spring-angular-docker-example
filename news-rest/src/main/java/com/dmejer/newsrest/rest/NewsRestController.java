package com.dmejer.newsrest.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dmejer.newsrest.api.NewsApiService;
import com.dmejer.newsrest.api.exception.BadRequestNewsException;
import com.dmejer.newsrest.api.exception.InternalErrorNewsException;
import com.dmejer.newsrest.api.exception.WrongApiKeyNewsException;
import com.dmejer.newsrest.dto.News;

@RestController
public class NewsRestController {

	@Autowired
	private NewsApiService newsConsumerService;

	@RequestMapping(value = "/news/{country}/{category}", method = RequestMethod.GET)
	@ResponseBody
	@CrossOrigin
	public ResponseEntity<News> getAllNews(@PathVariable String country, @PathVariable String category,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer size)
			throws WrongApiKeyNewsException, BadRequestNewsException, InternalErrorNewsException {

		News news = newsConsumerService.getAllNews(country, category, page, size);
		return new ResponseEntity<>(news, HttpStatus.OK);
	}

}
