import { Article } from "./article";

export class News {

    status: string;
    totalResults: number;
    articles: Article[];

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }


}
