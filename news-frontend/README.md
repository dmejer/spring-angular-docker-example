# NewsFrontend

News-frontend module prints data from from news-backend module in html table.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. `npm start` creates proxy on `/news/*` to connect news-rest module on `http://localhost:8080`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Running docker image

Run 
```sh
$ docker run -p 80:80 -e NEWS_API_URL="http://192.168.0.104:8080" -it registry.gitlab.com/dmejer/spring-angular-docker-example/news-frontend::master
```
Change NEWS_API_URL to point news-rest module.